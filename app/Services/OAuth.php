<?php

namespace App\Services;

use OAuth2\GrantType\UserCredentials;
use OAuth2\Server;
use OAuth2\Request;
use App\Models\OAuthModel;

class OAuth
{
    public $server;

    function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $storage = new MyPdo(array('dsn' => 'pgsql:host=ec2-52-86-25-51.compute-1.amazonaws.com;port=5432;dbname=d5540ppcg0u20g;user=ariklyvkmaoikg;password=1bd8dda240d8936eb7a5526c042a9e46a2e7418cb85f39f9e22c5d1e4c5b8014'),array('user_table' => 'users'));
        $grantType = new UserCredentials($storage);
        $this->server = new Server($storage);
        $this->server->addGrantType($grantType);
    }

    public function isLoggedIn()
    {
        return $this->server->verifyResourceRequest(Request::createFromGlobals());
    }

}
