<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class recite extends Seeder
{
    public function run()
    {
        $data = [
            'name_cat' => 'Завтрак',
        ];
        $this->db->table('category')->insert($data);

        $data = [
            'name_cat' => 'Обед',
        ];
        $this->db->table('category')->insert($data);

        $data = [
            'name_cat' => 'Ужин',
        ];
        $this->db->table('category')->insert($data);

        $data = [
            'name_cat' => 'Десерт',
        ];
        $this->db->table('category')->insert($data);

        $data = [
            'name_cat' => 'Перекус',
        ];
        $this->db->table('category')->insert($data);


        $data = [
            'name' => 'Крупа манная',
            'units' => '100г',
        ];
        $this->db->table('ingredient')->insert($data);

        $data = [
            'name' => 'Молоко',
            'units' => '250мл',
        ];
        $this->db->table('ingredient')->insert($data);


        $data = [
            'name' => 'Манна каша',
            'cookingmethod' => 'Варить',
            'time' => '20:00',
            'id_category' => 1,
        ];
        $this->db->table('dish')->insert($data);

        $data = [
            'name' => 'Куриный суп',
            'cookingmethod' => 'Варить',
            'time' => '1:00:00',
            'id_category' => 2,
        ];
        $this->db->table('dish')->insert($data);


        $data = [
            'quantity' => '1',
            'id_dish' => 1,
            'id_ingredient' => 1
        ];
        $this->db->table('recipe')->insert($data);

        $data = [
            'quantity' => '2',
            'id_dish' => 2,
            'id_ingredient' => 2
        ];
        $this->db->table('recipe')->insert($data);

    }
}
