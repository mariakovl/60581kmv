<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Dish extends Migration
{
    public function up()
    {
        if (!$this->db->tableexists('category')) {
            $this->forge->addkey('category_id', TRUE);

            $this->forge->addfield(array(
                'category_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name_cat' => array('type' => 'VARCHAR', 'constraint' => '225', 'null' => FALSE),
            ));
            $this->forge->createtable('category', TRUE);
        }

        if (!$this->db->tableexists('ingredient')) {
            $this->forge->addkey('ingredient_id', TRUE);

            $this->forge->addfield(array(
                'ingredient_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '225', 'null' => FALSE),
                'units' => array('type' => 'VARCHAR', 'constraint' => '225', 'null' => FALSE),
            ));
            $this->forge->createtable('ingredient', TRUE);
        }

        if (!$this->db->tableexists('dish')) {
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_category' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => FALSE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '225', 'null' => FALSE),
                'cookingmethod' => array('type' => 'VARCHAR', 'constraint' => '225', 'null' => FALSE),
                'time' => array('type' => 'TIME', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('id_category', 'category', 'category_id', 'RESRICT', 'RESRICT');

            $this->forge->createtable('dish', TRUE);
        }


        // activity_type
        if (!$this->db->tableexists('recipe')) {
            // Setup Keys
            $this->forge->addkey('recipe_id', TRUE);

            $this->forge->addfield(array(
                'recipe_id' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'id_dish' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'id_ingredient' => array('type' => 'INT', 'null' => FALSE, 'unsigned' => TRUE),
                'quantity' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_dish', 'dish', 'id', 'RESRICT', 'RESRICT');
            $this->forge->addForeignKey('id_ingredient', 'ingredient', 'ingredient_id', 'RESRICT', 'RESRICT');

            // create table
            $this->forge->createtable('recipe', TRUE);
        }


    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->droptable('recipe');
        $this->forge->droptable('dish');
        $this->forge->droptable('category');
        $this->forge->droptable('ingredient');
    }
}

