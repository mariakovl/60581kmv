<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddPictureUrlFiel extends Migration
{
    public function up()
    {
        if ($this->db->tableexists('dish')) {
            $this->forge->addColumn('dish', array(
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
    }

    public function down()
    {
        $this->forge->dropColumn('dish', 'picture_url');
    }
}
