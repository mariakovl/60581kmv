<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="jumbotron text-center">
    <img class="mb-4" src='/bear.jpg' alt="" width="200" height="200">
    <h1 class="display-4">Recipe</h1>
    <p class="lead">Это приложение поможет создавать рецепты блюд и смотреть рецепты других пользователей.</p>
    <?php if (!$ionAuth->loggedIn()): ?>
        <a class="btn btn-primary btn-lg" href="/auth/login" role="button">Войти</a>
    <?php else: ?>
        <a class="btn btn-primary btn-lg" href="/dish" role="button">Посмотреть рецепты</a>
    <?php endif ?>
</div>
<?= $this->endSection() ?>

