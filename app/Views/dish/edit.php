<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('dish/store'); ?>
    <input type="text" name="id" value="<?= $dish["id"] ?>">


    <div class="form-group">
        <label for="name">Название</label>
        <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
               value="<?= old('name'); ?>">
        value="<?= $dish["name"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('name') ?>
        </div>
    </div>


    <div class="form-group">
        <label class="form-check-label">Категория</label>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="id_category"
                   value="0" <?= old('id_category') == '0' ? 'checked' : '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Завтрак</small>
            </label>
        </div>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="id_category"
                   value="1" <?= old('id_category') == '1' ? 'checked' : '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Обед</small>
            </label>

        </div>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="id_category"
                   value="2" <?= old('id_category') == '1' ? 'checked' : '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Ужин</small>
            </label>

        </div>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="id_category"
                   value="3" <?= old('id_category') == '1' ? 'checked' : '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Десерт</small>
            </label>

        </div>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="id_category"
                   value="4" <?= old('id_category') == '1' ? 'checked' : '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Перекус</small>
            </label>

        </div>
        <div class="invalid-feedback" style="display: block">
            <?= $validation->getError('id_category') ?>
        </div>
    </div>


    <div class="form-group">
        <label class="form-check-label">Метод приготовления</label>
        <div class="form-check ">
            <input class="form-check-input" type="checkbox" name="cookingmethod"
                   value="0" <?= old('cookingmethod') == '0' ? 'checked' : '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Варить</small>
            </label>
        </div>
        <div class="form-check ">
            <input class="form-check-input" type="checkbox" name="cookingmethod"
                   value="1" <?= old('cookingmethod') == '1' ? 'checked' : '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Жарить</small>
            </label>

        </div>
        <div class="form-check ">
            <input class="form-check-input" type="checkbox" name="cookingmethod"
                   value="2" <?= old('cookingmethod') == '1' ? 'checked' : '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Тушить</small>
            </label>

        </div>
        <div class="form-check ">
            <input class="form-check-input" type="checkbox" name="cookingmethod"
                   value="3" <?= old('cookingmethod') == '1' ? 'checked' : '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Выпекать</small>
            </label>

        </div>
        <div class="invalid-feedback" style="display: block">
            <?= $validation->getError('cookingmethod') ?>
        </div>
    </div>


    <div class="form-group">
        <label for="time">Время приготовления</label>
        <input type="time" class="form-control <?= ($validation->hasError('time')) ? 'is-invalid' : ''; ?>" name="time"
               value="<?= old('time'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('time') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="birthday">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>"
               name="picture">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>


</div>
<?= $this->endSection() ?>
