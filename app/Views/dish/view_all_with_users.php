<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php if (!empty($dish) && is_array($dish)) : ?>
        <h2>Все блюда:</h2>
        <div class="d-flex justify-content-between mb-2">
            <?= $pager->links('group1', 'my_page') ?>
            <?= form_open('dish/viewAllWithUsers', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
            </form>
            <?= form_open('dish/viewAllWithUsers', ['style' => 'display: flex']); ?>
            <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание"
                   aria-label="Search"
                   value="<?= $search; ?>">
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
            </form>
        </div>
        <table class="table table-striped">
            <thead>
            <th scope="col">Фотография</th>
            <th scope="col">Название</th>
            <th scope="col">Категория</th>
            <th scope="col">Время приготовления</th>
            <th scope="col">Ингридиенты</th>
            <th scope="col">Описание</th>
            <th scope="col">Управление</th>
            </thead>
            <tbody>
            <?php foreach ($dish as $item): ?>
                <tr>
                    <td>

                        <?php switch ($item['id']) {
                            case 1: ?>
                                <img height="150" width="150"
                                     src="https://img.povar.ru/main/48/d6/91/c4/mannaya_kasha_na_kokosovom_moloke-578337.jpeg"
                                     alt="<?= esc($item['name']); ?>">
                                <?php break;
                            case 2: ?>
                                <img height="150" width="150"
                                     src="https://i.pinimg.com/564x/3f/c2/79/3fc27986c568eeb699a9e5116e4031b1.jpg"
                                     alt="<?= esc($item['name']); ?>">
                                <?php break;
                            case 3: ?>
                                <img height="150" width="150"
                                     src="https://i.pinimg.com/originals/d3/0f/33/d30f33f1b4689e8a13dc1116a974801b.jpg"
                                     alt="<?= esc($item['name']); ?>">
                                <?php break;
                            case 4: ?>
                                <img height="150" width="150"
                                     src="https://www.lecreuset.co.za/recipes/wp-content/uploads/2014/12/Lamb-Knuckle-Large-554x554.jpg"
                                     alt="<?= esc($item['name']); ?>">
                                <?php break;
                            case 5: ?>
                                <img height="150" width="150"
                                     src="https://i1.wp.com/smittenkitchen.com/wp-content/uploads//2015/01/my-ultimate-chicken-noodle-soup1.jpg?fit=750%2C500&ssl=1"
                                     alt="<?= esc($item['name']); ?>">
                                <?php break;
                            case 6: ?>
                                <img height="150" width="150"
                                     src="https://64.media.tumblr.com/e62e07730cfb1dd63b28b1cc2a3048cd/tumblr_oneq9bHEOS1rb7kzdo1_1280.jpg"
                                     alt="<?= esc($item['name']); ?>">
                                <?php break;
                            case 7: ?>
                                <img height="150" width="150"
                                     src="https://i.pinimg.com/564x/d1/5c/44/d15c442f4fa7144f9bfc01e265e5824b.jpg"
                                     alt="<?= esc($item['name']); ?>">
                                <?php break;
                            case 8: ?>
                                <img height="150" width="150"
                                     src="https://i.pinimg.com/564x/d2/86/a5/d286a55b1635bbf1d4c57233f39e3762.jpg"
                                     alt="<?= esc($item['name']); ?>">
                                <?php break;
                            case 9: ?>
                                <img height="150" width="150"
                                     src="https://i.pinimg.com/564x/8c/9d/b8/8c9db8a95abff7b93a1e0caac48b6f04.jpg"
                                     alt="<?= esc($item['name']); ?>">
                                <?php break;
                            default:
                                echo 'Изображение отсутствует';
                                break;
                        }
                        ?>

                    </td>
                    <td><?= esc($item['name']); ?></td>
                    <td><?= esc($item['name_cat']); ?></td>
                    <td><?= esc($item['time']); ?></td>
                    <td><?= esc($item['time']); ?></td>
                    <td><?= esc($item['time']); ?></td>

                    <td>
                        <a href="<?= base_url() ?>/dish/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                        <a href="<?= base_url() ?>/dish/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                        <a href="<?= base_url() ?>/dish/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                    </td>

                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    <?php else : ?>
        <div class="text-center">
            <p>Блюдане найдены </p>
            <a class="btn btn-primary btn-lg" href="<?= base_url() ?>/dish/create"><span class="fas fa-tachometer-alt"
                                                                                         style="color:white"></span>&nbsp;&nbsp;Создать
                блюдо</a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
