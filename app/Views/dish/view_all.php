<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <h2>Все рецепты</h2>

    <?php if (!empty($dish) && is_array($dish)) : ?>

        <?php foreach ($dish as $item): ?>

            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">

                        <?php if (is_null($item['picture_url'])) : ?>
                            <?php switch ($item['id']) {
                                case 1: ?>
                                    <img height="150" width="150"
                                         src="https://img.povar.ru/main/48/d6/91/c4/mannaya_kasha_na_kokosovom_moloke-578337.jpeg"
                                         alt="<?= esc($item['name']); ?>">
                                    <?php break;
                                case 2: ?>
                                    <img height="150" width="150"
                                         src="https://i.pinimg.com/564x/3f/c2/79/3fc27986c568eeb699a9e5116e4031b1.jpg"
                                         alt="<?= esc($item['name']); ?>">
                                    <?php break;
                                case 3: ?>
                                    <img height="150" width="150"
                                         src="https://i.pinimg.com/originals/d3/0f/33/d30f33f1b4689e8a13dc1116a974801b.jpg"
                                         alt="<?= esc($item['name']); ?>">
                                    <?php break;
                                case 4: ?>
                                    <img height="150" width="150"
                                         src="https://www.lecreuset.co.za/recipes/wp-content/uploads/2014/12/Lamb-Knuckle-Large-554x554.jpg"
                                         alt="<?= esc($item['name']); ?>">
                                    <?php break;
                                case 5: ?>
                                    <img height="150" width="150"
                                         src="https://i1.wp.com/smittenkitchen.com/wp-content/uploads//2015/01/my-ultimate-chicken-noodle-soup1.jpg?fit=750%2C500&ssl=1"
                                         alt="<?= esc($item['name']); ?>">
                                    <?php break;
                                case 6: ?>
                                    <img height="150" width="150"
                                         src="https://64.media.tumblr.com/e62e07730cfb1dd63b28b1cc2a3048cd/tumblr_oneq9bHEOS1rb7kzdo1_1280.jpg"
                                         alt="<?= esc($item['name']); ?>">
                                    <?php break;
                                case 7: ?>
                                    <img height="150" width="150"
                                         src="https://i.pinimg.com/564x/d1/5c/44/d15c442f4fa7144f9bfc01e265e5824b.jpg"
                                         alt="<?= esc($item['name']); ?>">
                                    <?php break;
                                case 8: ?>
                                    <img height="150" width="150"
                                         src="https://i.pinimg.com/564x/d2/86/a5/d286a55b1635bbf1d4c57233f39e3762.jpg"
                                         alt="<?= esc($item['name']); ?>">
                                    <?php break;
                                case 9: ?>
                                    <img height="150" width="150"
                                         src="https://i.pinimg.com/564x/8c/9d/b8/8c9db8a95abff7b93a1e0caac48b6f04.jpg"
                                         alt="<?= esc($item['name']); ?>">
                                    <?php break;
                                default:
                                    echo 'Изображение отсутствует';
                                    break;
                            }
                            ?>
                        <?php else: ?>
                            <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img"
                                 alt="<?= esc($item['name']); ?>">
                        <?php endif ?>


                    </div>

                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($item['name']); ?></h5>
                            <p class="card-text"><a>Время приготовления: </a><?= esc($item['time']); ?></p>
                            <a href="<?= base_url() ?>/index.php/dish/view/<?= esc($item['id']); ?>"
                               class="btn btn-primary">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    <?php else : ?>
        <p>Невозможно найти рецепт.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
