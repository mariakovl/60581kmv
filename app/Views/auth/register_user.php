<?php

use Config\Services;

?>
<?= $this->extend('templates/layout');
echo strlen($message) ?>
<?= $this->section('content') ?>

<div class="container mt-5 mb-5">
    <div class="row justify-content-center bg-light pt-3 pb-3 border mb-3 shadow-sm rounded">
        <div class="col-md-6 col-12 text-center">
            <h1>Регистрация</h1>
            <?php if (isset($message)): ?>
                <div class="aalert alert-danger text-center">
                    <?php echo $message; ?>
                </div>
            <?php endif ?>
            <?php echo form_open('auth/register_user'); ?>
            <div class="mb-3">
                Имя <br/>
                <?php echo form_input($first_name); ?>
            </div>
            <div class="mb-3">
                Фамилия <br/>
                <?php echo form_input($last_name); ?>
            </div>
            <?php
            if ($identity_column !== 'email') {
                echo '<div class="mb-3">';
                echo form_label(lang('Auth.create_user_identity_label'), 'identity');
                echo '<br/>';
                echo Services::validation()->getError('identity');
                echo form_input($identity);
                echo '</div>';
            }
            ?>
            <div class="mb-3">
                Компания <br/>
                <?php echo form_input($company); ?>
            </div>
            <div class="mb-3">
                Почта <br/>
                <?php echo form_input($email); ?>
            </div>
            <div class="mb-3">
                Телефон <br/>
                <?php echo form_input($phone); ?>
            </div>
            <div class="mb-3">
                Пароль<br/>
                <?php echo form_input($password); ?>
            </div>
            <div class="mb-3">
                Подтвердите пароль <br/>
                <?php echo form_input($password_confirm); ?>
            </div>
            <div class="mb-3">
                <?php echo form_submit('submit', lang('Зарегистрироваться')); ?>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

