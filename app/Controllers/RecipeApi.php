<?php

namespace App\Controllers;


use Aws\S3\S3Client;

use App\Models\DishModel;
use CodeIgniter\RESTful\ResourceController;
use App\Services\OAuth;
use OAuth2\Request;
use App\Models\OAuthModel;
use App\Models\CategoriesModel;

class RecipeApi extends ResourceController
{
    protected $modelName = 'App\Models\DishModel';
    protected $format = 'json';
    protected $oauth;

    public function __construct()
    {
        Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
        Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
        Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method
    }

    public function recipe() //Отображение всех записей
    {
        $oauth = new OAuth();
        if ($oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            $per_page = $this->request->getPost('per_page');
            $search = $this->request->getPost('search');
            $data = $this->model->getDishWithUser($OAuthModel->getUser()->user_id == 'administrator' ? null : $OAuthModel->getUser()->id, $search, $per_page);
            //Ответ контроллера включает данные (ratings) и параметры пагинации (pager)
            return $this->respond(['dish' => $data, 'pager' => $model->pager->getDetails('group1')]);

        }
        $oauth->server->getResponse()->send();
    }

    public function categories() //Отображение всех записей
    {
        $oauth = new OAuth();
        if ($oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = new CategoriesModel();
            $data = $model->getCategory();
            //Ответ контроллера включает данные (ratings) и параметры пагинации (pager)
            return $this->respond(['categories' => $data]);

        }
        $oauth->server->getResponse()->send();
    }

    public function findRecipe() //Отображение всех записей
    {
        $oauth = new OAuth();
        if ($oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            $per_page = $this->request->getPost('per_page');
            $current_page = $this->request->getPost('current_page');
            $search = $this->request->getPost('search');
            $data = $this->model->findDish($per_page, $current_page, $search);
            //Ответ контроллера включает данные (ratings) и параметры пагинации (pager)
            return $this->respond(['dish' => $data, 'perPage' => $per_page, 'page' => $current_page, 'search' => $search]);

        }
        $oauth->server->getResponse()->send();
    }

    public function recipeCount() //Отображение всех записей маша
    {
        $oauth = new OAuth();
        $OAuthModel = new OAuthModel();
        $model = $this->model;
        $search = $this->request->getPost('search');
        $data = $this->model->getDishCount($search);
        //Ответ контроллера включает данные (ratings) и параметры пагинации (pager)
        return $this->respond(['count' => $data]);
    }

    public function store()
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {

            $model = $this->model;


            if ($this->request->getMethod() === 'post' && $this->validate([
                    'name' => 'required|min_length[3]|max_length[255]',
                    'id_category' => 'required',
                    'cookingmethod' => 'required',
                    'time' => 'required',
                    //'picture'  => 'is_image[picture]|max_size[picture,12000]',
                ]))
            {
                $insert = null;

                $file = $this->request->getFile('picture');
                if ($file->getSize() != 0) {

                    //подключение хранилища

                    $s3 = new S3Client([
                        'version' => 'latest',
                        'region' => 'us-east-1',
                        'endpoint' => getenv('S3_ENDPOINT'),
                        'use_path_style_endpoint' => true,
                        'credentials' => [
                            'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                            'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                        ],
                    ]);


                    //получение расширения имени загруженного файла
                    $ext = explode('.', $file->getName());
                    $ext = $ext[count($ext) - 1];

                    //загрузка файла в хранилище
                    $insert = $s3->putObject([
                        'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                        //генерация случайного имени файла
                        'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                        'Body' => fopen($file->getRealPath(), 'r+'),
                        //'@use_path_style_endpoint' => true

                    ]);

                }



                $new_data = [
                    'name' => $this->request->getPost('name'),
                    'id_category' => ($this->request->getPost('id_category') + 1),
                    'cookingmethod' => $this->request->getPost('cookingmethod'),
                    'time' => $this->request->getPost('time'),
                ];

                if (!is_null($insert))
                    $new_data['picture_url'] = $insert['ObjectURL'];
                else{
                    $model->save($new_data);
                    return $this->respondCreated(null, 'S3 Link error. Empty poster url. Movie created successfully');
                }

                $model->save($new_data);
                //-----------------------------
                return $this->respondCreated(null, 'Movie created successfully');
            } else {
                return $this->respond($this->validator->getErrors());
            }
        } else $this->oauth->server->getResponse()->send();
    }


    public function delete($id = null)
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isloggedIn()) {

            $model = $this->model;
            $model->delete($id);
            return $this->respondDeleted(null, 'Dish deleted successfully');
        }

        $this->oauth->server->getResponse()->send();
    }
}
