<?php namespace App\Models;

use CodeIgniter\Model;
use function Webmozart\Assert\Tests\StaticAnalysis\length;

class DishModel extends Model
{
    protected $table = 'dish'; //таблица, связанная с моделью
    protected $allowedFields = ['name', 'id_category', 'cookingmethod', 'time', 'picture_url'];

    public function getDish($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getDishWithUser($id = null, $search = '', $per_page = null)
    {
        $builder = $this->select('*')->like('name', is_null($search) ? '' : $search, 'both');
        if (!is_null($id)) {
            $builder=$builder->where(['dish.id' => $id])->first();
        }
        // Пагинация
        return $builder->paginate($per_page, 'group1');
    }

    public function findDish($per_page = 0, $current_page = null, $search = '')
    {
        $count = $current_page == null ? 0 : $current_page * $per_page;
        return $this->select('*')->like('name', is_null($search) ? '' : $search, 'both')->findAll($per_page, $per_page * $current_page);
    }

    public function getDishCount($search = ''){
        return $this->select('count(*)')->like('name', is_null($search) ? '' : $search, 'both')->first();
    }
}
