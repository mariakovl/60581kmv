<?php namespace App\Models;

use CodeIgniter\Model;
use function Webmozart\Assert\Tests\StaticAnalysis\length;

class CategoriesModel extends Model
{
    protected $table = 'category'; //таблица, связанная с моделью
    protected $allowedFields = ['category_id', 'name_cat'];

    public function getCategory()
    {
        return $this->findAll();
    }
}
