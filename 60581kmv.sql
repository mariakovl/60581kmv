-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Апр 19 2021 г., 12:07
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60581kmv`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `category_id (PK)` int NOT NULL COMMENT 'категория id',
  `name` varchar(255) NOT NULL COMMENT 'наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`category_id (PK)`, `name`) VALUES
(1, 'Завтрак'),
(2, 'Обед'),
(3, 'Ужин'),
(4, 'Десерт'),
(5, 'Перекус');

-- --------------------------------------------------------

--
-- Структура таблицы `dish`
--

CREATE TABLE `dish` (
  `dish_id (PK)` int NOT NULL,
  `id_category(FK)` int NOT NULL COMMENT 'id категории',
  `name` varchar(255) NOT NULL COMMENT 'наименование',
  `cookingmethod` varchar(255) NOT NULL COMMENT 'способ приготовления',
  `time` time NOT NULL COMMENT 'время приготовления, мин'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dish`
--

INSERT INTO `dish` (`dish_id (PK)`, `id_category(FK)`, `name`, `cookingmethod`, `time`) VALUES
(1, 1, 'Манная каша', 'Варить', '00:20:00'),
(2, 1, 'Пшеничная каша', 'Варить', '00:15:00');

-- --------------------------------------------------------

--
-- Структура таблицы `ingredient`
--

CREATE TABLE `ingredient` (
  `ingredient_id (PK)` int NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'наименование',
  `units` varchar(255) NOT NULL COMMENT 'единицы измерения'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ingredient`
--

INSERT INTO `ingredient` (`ingredient_id (PK)`, `name`, `units`) VALUES
(1, 'Крупа манная', '100г'),
(2, 'Молоко', '250 мл'),
(3, 'Соль, сахар', 'По вкусу');

-- --------------------------------------------------------

--
-- Структура таблицы `recipe`
--

CREATE TABLE `recipe` (
  `recipe_id(PK)` int NOT NULL,
  `id_dish (FK)` int NOT NULL,
  `id_ingridient (FK)` int NOT NULL,
  `quantity` int NOT NULL COMMENT 'количество'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `recipe`
--

INSERT INTO `recipe` (`recipe_id(PK)`, `id_dish (FK)`, `id_ingridient (FK)`, `quantity`) VALUES
(1, 1, 1, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id (PK)`);

--
-- Индексы таблицы `dish`
--
ALTER TABLE `dish`
  ADD PRIMARY KEY (`dish_id (PK)`),
  ADD KEY `id_category(FK)` (`id_category(FK)`);

--
-- Индексы таблицы `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`ingredient_id (PK)`);

--
-- Индексы таблицы `recipe`
--
ALTER TABLE `recipe`
  ADD KEY `id_dish (FK)` (`id_dish (FK)`),
  ADD KEY `id_ingridient (FK)` (`id_ingridient (FK)`);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `dish`
--
ALTER TABLE `dish`
  ADD CONSTRAINT `dish_ibfk_1` FOREIGN KEY (`id_category(FK)`) REFERENCES `category` (`category_id (PK)`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `recipe`
--
ALTER TABLE `recipe`
  ADD CONSTRAINT `recipe_ibfk_1` FOREIGN KEY (`id_dish (FK)`) REFERENCES `dish` (`dish_id (PK)`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `recipe_ibfk_2` FOREIGN KEY (`id_ingridient (FK)`) REFERENCES `ingredient` (`ingredient_id (PK)`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
